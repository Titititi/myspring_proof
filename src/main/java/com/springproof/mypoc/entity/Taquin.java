package com.springproof.mypoc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "taquin")
public class Taquin {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;
 
    @Column(name = "line_taquin", length = 10, nullable = false)
    private String fullName;

    public Taquin(String fN) {
        this.fullName = fN;
    }
}
