package com.springproof.mypoc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "taquin_solv")
public class TaquinSolv {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_taquin_ref")
    private Taquin taquin;
 
    // Représente le numéro de l'itération pour une résolution 
    @Column(name = "step")
    private Long stepNum;

    @Column(name = "line_taquin", length = 10, nullable = false)
    private String fullName;
}
