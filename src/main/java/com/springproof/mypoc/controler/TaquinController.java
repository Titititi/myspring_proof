package com.springproof.mypoc.controler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.springproof.mypoc.dto.TaquinDTO;
import com.springproof.mypoc.entity.Taquin;
import com.springproof.mypoc.entity.TaquinSolv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/taquin")
public class TaquinController {

    @Autowired
    private TaquinDTO taquinDTO;

    @GetMapping(value = "/{taquinId}")
    public List<TaquinSolv> getTaquinSolv(@PathVariable Long taquinId) {

        
        return taquinDTO.solveOneTaquin(taquinId);
    }

    @PostMapping(value = "/{taquinLine}")
    public Taquin postTaquin(@PathVariable String taquinLine) {
        Taquin t = new Taquin(taquinLine);
        taquinDTO.save(t);
        return t;
    }



    
}
