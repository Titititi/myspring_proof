package com.springproof.mypoc.dao;

import java.util.Optional;

import com.springproof.mypoc.entity.Taquin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaquinDAO extends CrudRepository<Taquin, Long> {

    public Optional<Taquin> findById(Long id);
    
}
