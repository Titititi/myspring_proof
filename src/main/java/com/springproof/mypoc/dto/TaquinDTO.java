package com.springproof.mypoc.dto;

import java.util.List;
import java.util.Optional;

import com.springproof.mypoc.dao.TaquinDAO;
import com.springproof.mypoc.entity.Taquin;
import com.springproof.mypoc.entity.TaquinSolv;

import org.springframework.beans.factory.annotation.Autowired;

public class TaquinDTO {
    
    @Autowired
    private TaquinDAO taquinDAO;

    public List<TaquinSolv> solveOneTaquin(Long id) {
        Optional<Taquin> currentTaquin = taquinDAO.findById(id);

        //solve Taquin arbitrary and get List<TaquinSolv>
        //+write mockitotest

        return null;
    }

	public void save(Taquin t) {
        taquinDAO.save(t);
	}

}
